<div class="menu">
            <div class="container _2">
              <div class="navigation-content">
                <a href="/" class="navigation-first _1 w-inline-block" style="text-decoration: none">
                  <h1 class="navigation-text _2">INICIO</h1>
                  <div class="navigation-line"></div>
                </a>
                <a href="/nosotros" aria-current="page" class="navigation-first _2 w-inline-block" style="text-decoration: none">
                  <h1 class="navigation-text _3">NOSOTROS</h1>
                  <div class="navigation-line"></div>
                </a>
                <a href="/proyectos" class="navigation-first _3 w-inline-block" style="text-decoration: none">
                  <h1 class="navigation-text _4">PROYECTOS</h1>
                  <div class="navigation-line"></div>
                </a>
                <a href="/blogs" class="navigation-first _3 w-inline-block" style="text-decoration: none">
                  <h1 class="navigation-text _4">Blog</h1>
                  <div class="navigation-line"></div>
                </a>
                <a href="/contactanos" class="navigation-first _4 w-inline-block" style="text-decoration: none">
                  <h1 class="navigation-text _5">CONTACTO</h1>
                  <div class="navigation-line"></div>
                </a>
                <div class="margin-60px">
                  <div class="menu-move">
                    <div class="contact-flex-1">
                      <h3 class="uppercase-text">UBÍCANOS</h3>
                      <div>
                        <p class="menu-paragrapg">Av Lima 100, San Isidro,<br>Lima Perú</p>
                      </div>
                    </div>
                    <div>
                      <div class="contact-flex-2">
                        <div class="contact-big-content">
                          <div class="contact-icon"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625973f61d15bdd4e824f05f_phone.png" loading="lazy" width="30" alt="" class="image"></div>
                          <a href="tel:+1-202-555-0104" class="title-5 contact-title">+51 230 3000</a>
                        </div>
                        <div class="contact-big-content">
                          <div class="contact-icon"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625973f61d15bd45f524f05d_mail.png" loading="lazy" width="30" alt="" class="image"></div>
                          <a href="mailto:info@atra.com" class="title-5 contact-title">info@rubika.com</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>