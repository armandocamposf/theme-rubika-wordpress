<div class="footer wf-section">
      <div class="container _3">
        <div data-w-id="7202b0ad-a23a-3891-723d-3993969f821e" class="top-footer-part">
          <div id="w-node-_7202b0ad-a23a-3891-723d-3993969f821f-969f821c"><img src="<?php bloginfo('template_directory') ?>/images/logo-verlight.png" loading="lazy" width="941.5" alt="" class="logo-footer"></div>
          <div id="w-node-_7202b0ad-a23a-3891-723d-3993969f8221-969f821c">
            <a href="contacto.html" class="button w-button">CONTÁCTANOS</a>
          </div>
        </div>
        <div class="margin-30px">
          <div class="grid-3-columns">
            <div id="w-node-_7202b0ad-a23a-3891-723d-3993969f8226-969f821c" data-w-id="7202b0ad-a23a-3891-723d-3993969f8226">
              <div class="margin-30px">
                <div class="contact-grid on-footer">
                  <div></div>
                  <div></div>
                </div>
              </div>
            </div>
            <div id="w-node-_7202b0ad-a23a-3891-723d-3993969f8235-969f821c" data-w-id="7202b0ad-a23a-3891-723d-3993969f8235">
              <div class="margin-30px">
                <div class="contact-grid on-footer">
                  <div></div>
                  <div></div>
                </div>
              </div>
            </div>
            <div id="w-node-_7202b0ad-a23a-3891-723d-3993969f8244-969f821c" data-w-id="7202b0ad-a23a-3891-723d-3993969f8244">
              <h3 class="footer-title">Lima, Perú</h3>
              <div class="margin-30px">
                <div class="contact-grid on-footer">
                  <div>
                    <p class="paragraph-white">Av. Lima 100, Lima Perú<br></p>
                  </div>
                  <div>
                    <a href="#" class="paragraph-link-1">TEL: +511 430 5659</a>
                    <a href="#" class="paragraph-link-1">E: INFO@RUBIKA.com</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="margin-50px">
            <div class="margin-40px">
              <div class="copyright-flex">
                <p class="copyright">© Rubika 2023<a href="https://webflow.com/" target="_blank" class="white-link-2"></a>
                </p>
                <div id="w-node-_7202b0ad-a23a-3891-723d-3993969f825d-969f821c" class="social-grid">
                  <a href="#" class="social-link-1 w-inline-block"><img src="https://uploads-ssl.webflow.com/61d7143a2905d273d92bb172/61d831296a4c64046f564735_facebook.png" loading="lazy" alt=""></a>
                  <a href="#" class="social-link-1 w-inline-block"><img src="https://uploads-ssl.webflow.com/61d7143a2905d273d92bb172/61d8312abe9d8d754aba8c37_instagram.png" loading="lazy" alt=""></a>
                  <a href="#" class="social-link-1 w-inline-block"><img src="https://uploads-ssl.webflow.com/61d7143a2905d273d92bb172/61d83129e08587f8794ad9e5_linkedin.png" loading="lazy" alt=""></a>
                  <a href="#" class="social-link-1 w-inline-block"><img src="https://uploads-ssl.webflow.com/61d7143a2905d273d92bb172/61d831296d1a1a9a233f2f6f_twiiter.png" loading="lazy" alt=""></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <a href="#Top" style="-webkit-transform:translate3d(150%, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(150%, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(150%, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(150%, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)" class="button-top go-top w-inline-block"><img src="https://uploads-ssl.webflow.com/624c4b185ef5f6159887042a/624fe91f72cb26d1a489835c_arrow_drop_up_white_24dp%20(1).svg" loading="lazy" width="28" alt="" class="arrow-img"></a>
    <div class="loader">
      <div class="loading-flex">
        <div class="div-block"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/62596961b8f7d1770b41bdae_tail-spin.svg" loading="lazy" width="22" alt="" class="rotator">
          <div class="loadin-flex"><img src="images/FRIDAY-PARTY.png" loading="lazy" width="200" alt=""></div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=63e1d88fdd964a64f8ae787b" type="text/javascript" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="<?php bloginfo('template_directory') ?>/js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
<?php wp_footer(); ?>
</html>