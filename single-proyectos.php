<!DOCTYPE html><!--  This site was created in Webflow. https://www.webflow.com  -->
<!--  Last Published: Thu Feb 23 2023 17:19:44 GMT+0000 (Coordinated Universal Time)  -->
<html data-wf-page="63e1d890dd964a531fae7898" data-wf-site="63e1d88fdd964a64f8ae787b">
<head>
  <meta charset="utf-8">
  <title><?php wp_title(); ?></title>
  <meta content="" name="description">
  <meta content="" property="og:title">
  <meta content="" property="og:description">
  <meta content="" property="og:image">
  <meta content="" property="twitter:title">
  <meta content="" property="twitter:description">
  <meta content="" property="twitter:image">
  <meta property="og:type" content="website">
  <meta content="summary_large_image" name="twitter:card">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="<?php bloginfo('template_directory'); ?>/css/normalize.css" rel="stylesheet" type="text/css">
  <link href="<?php bloginfo('template_directory'); ?>/css/webflow.css" rel="stylesheet" type="text/css">
  <link href="<?php bloginfo('template_directory'); ?>/css/rubika-00645a.webflow.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com" rel="preconnect">
  <link href="https://fonts.gstatic.com" rel="preconnect" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Oswald:200,300,400,500,600,700","Exo:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Playfair Display:regular,500,600,700,800,900,italic,500italic,600italic,700italic,800italic,900italic"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php bloginfo('template_directory'); ?>/images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="<?php bloginfo('template_directory'); ?>/images/webclip.png" rel="apple-touch-icon">


  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

<style>
    #carrusel{float:left; width:588px; overflow:hidden; height:100px; position:relative; margin-top:20px;}
#carrusel .izquierda_flecha{position:absolute; left:10px; z-index:1; top:50%; margin-top:-9px;}
#carrusel .derecha_flecha{position:absolute; right:10px; z-index:1; top:50%; margin-top:-9px;}
.carrusel{width:4000px;left:0px; position:absolute; z-index:0}
.carrusel>div{
    float: left;
    height: 100px;
    margin-right: 5px;
    width: 195px;
    text-align:center;
}
.carrusel .img_carrusel{cursor:pointer;}

#content {
    float:left;
    width:600px;
    margin-bottom:40px;
    margin:0px auto;
}
</style>
</head>
<body class="body">
  <div id="Top" class="body-content">
    <div class="gradient-section">
      <div data-w-id="27d1603f-60fe-cd38-f160-01639a1c6297" class="hero inner proy nospace2">
        <div class="navigation-wrapper">
          <div class="navigation">
            <div class="logo-flex _2">
              <a href="index.html" class="logo-wrapper w-inline-block"><img src="<?php bloginfo('template_directory'); ?>/images/FRIDAY-PARTY.png" loading="lazy" width="80" alt="" class="constructo-logo"></a>
            </div>
            <div class="second-part">
              <div data-w-id="1b1828e5-ee8b-a7b3-1723-6ec8ef90000b" class="hamburger">
                <div class="hamburger-menu _2"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625965d7f934b827de7fd133_icon-menu.svg" loading="lazy" alt="" class="dots"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625965e24b264aeb52fab9eb_remove.png" loading="lazy" alt="" class="remove"></div>
                <div class="menu-text-wrapper">
                  <div class="menu-text">Menu</div>
                  <div class="menu-text">Cerrar</div>
                </div>
              </div>
            </div>
          </div>
          <div class="triangle-navigation"></div>
          <div class="triange-left left"></div>



          <?php get_template_part('template-parts/menu'); ?>




        </div>
        
        <div class="container">
          <div class="max-w-hero _2 make-center"></div>
        </div>
        <div data-w-id="27d1603f-60fe-cd38-f160-01639a1c629f" style="opacity:0" class="scroll-down-wrapper _1">
          <a href="#" class="scroll-flex w-inline-block">
            <div class="lottie-animation" data-w-id="27d1603f-60fe-cd38-f160-01639a1c62a1" data-animation-type="lottie" data-src="https://uploads-ssl.webflow.com/624c4b185ef5f6159887042a/624d9b4cfb0938ebedc5ecf5_lf30_editor_6govlks1.json" data-loop="1" data-direction="1" data-autoplay="1" data-is-ix2-target="0" data-renderer="svg" data-default-duration="3" data-duration="0"></div>
          </a>
        </div>
        <div class="tituloenproy">
          <div data-w-id="27d1603f-60fe-cd38-f160-01639a1c629b" style="opacity:0" class="title-1 project-page"><?php the_title(); ?></div>
        </div><img src="<?php bloginfo('template_directory'); ?>/images/path-2.png" loading="lazy" sizes="100vw" width="500" srcset="images/path-2-p-500.png 500w, images/path-2-p-800.png 800w, images/path-2-p-1080.png 1080w, images/path-2.png 1500w" alt="" class="path _2">
      </div>
    </div>

    <?php 
        while ( have_posts() ) :
            the_post();
            ?>
    <div class="section wf-section">
      <div class="container">



        <div class="blog-page-wrapper">
          <div class="text-wrapper _3">
            <div class="photo-interaction"><img src="" loading="eager" width="666.5" alt="" class="photo hide">
              <div class="absolute-background"></div>
            </div>
          </div>
          <div>
            <div class="rich-text-block w-richtext">
                <?php the_content(); ?>
            </div>
          </div>
          <div>
        <!-- Slider -->
        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
  </div>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="https://uploads-ssl.webflow.com/63e1d890dd964a7bf7ae78a7/63e4715c40b2f991ff316a26_IE-2025web-p-500.jpg" class="d-block w-100" alt="https://uploads-ssl.webflow.com/63e1d890dd964a7bf7ae78a7/63e4715c40b2f991ff316a26_IE-2025web-p-500.jpg">
    </div>
    <div class="carousel-item">
      <img src="https://uploads-ssl.webflow.com/63e1d890dd964a7bf7ae78a7/63e4715c40b2f991ff316a26_IE-2025web-p-500.jpg" class="d-block w-100" alt="https://uploads-ssl.webflow.com/63e1d890dd964a7bf7ae78a7/63e4715c40b2f991ff316a26_IE-2025web-p-500.jpg">
    </div>
    <div class="carousel-item">
      <img src="https://uploads-ssl.webflow.com/63e1d890dd964a7bf7ae78a7/63e4715c40b2f991ff316a26_IE-2025web-p-500.jpg" class="d-block w-100" alt="https://uploads-ssl.webflow.com/63e1d890dd964a7bf7ae78a7/63e4715c40b2f991ff316a26_IE-2025web-p-500.jpg">
    </div>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Anterior</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Siguiente</span>
  </button>
</div>
        <!-- Fin Slider -->
          </div>
        </div>
      </div>
    </div>
    <section class="team-slider-2 wf-section">
      <div class="services-flex">
        <div id="w-node-_4fa317d9-78bf-bd6e-6112-f99450560042-1fae7898">
          <div class="list-title _2">LUGAR</div>
          <p class="paragraph-2"><?= get_field('ubicacion'); ?></p>
        </div>
        <div>
          <div>
            <div class="list-title _2">ClientE</div>
            <p class="paragraph-3"><?= get_field('cliente'); ?></p>
          </div>
        </div>
        <div>
          <div>
            <div class="list-title _2">Año de entrega</div>
            <p class="paragraph-4"><?= get_field('ano_de_entrega'); ?></p>
          </div>
        </div>
        <div id="w-node-_0f717cff-c9a8-f70d-a27b-31a359e96b08-1fae7898">
          <div class="list-title _2">ESTADO</div>
          <p class="paragraph-5"><?= get_field('estado'); ?></p>
        </div>
        <div id="w-node-c0d5d557-60ae-e32a-f9b3-7e9534967f36-1fae7898">
          <div class="list-title _2">SOCIOS</div>
          <p class="paragraph-5"><?= get_field('socios'); ?></p>
        </div>
      </div>
    </section>
    <div class="section lines hide wf-section">
      <div>
        <div class="max-w-width _3">
          <h1 data-w-id="c5705b21-e814-2506-3578-67d588d58673" style="opacity:0" class="title-2"><span class="with-scribble">PROYECTOS</span></h1>
        </div>
      </div>
    </div>
    <?php endwhile; ?>
   
   
   
    

<div class="wf-section">
      <div class="div-block-6">
        <a href="/proyectos" class="boton23 w-button">Regresar a proyectos</a>
      </div>
    </div>
    <div data-w-id="cad5cb4a-77f4-ac20-92e5-f6ed5d42479e" class="section-cta image hero wf-section"><img src="images/SFONDO.png" loading="lazy" alt="" class="photo-section _3">
      <div class="container">
        <h1 data-w-id="cad5cb4a-77f4-ac20-92e5-f6ed5d4247a1" class="title-1 cta">SEGUIMOS CONTRIBUYENDO CON EL DESARROLLO DEL PERÚ</h1>
      </div>
    </div>
    <div class="loader">
      <div class="loading-flex">
        <div class="div-block"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/62596961b8f7d1770b41bdae_tail-spin.svg" loading="lazy" width="22" alt="" class="rotator">
          <div class="loadin-flex"><img src="<?php bloginfo('template_directory'); ?>/images/FRIDAY-PARTY.png" loading="lazy" width="200" alt=""></div>
        </div>
      </div>
    </div>
    <?php get_template_part('template-parts/footer'); ?>