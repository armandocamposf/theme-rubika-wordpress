<?php 
/*
    Template Name: Plantilla Inicio
*/ 

?>
<!DOCTYPE html><!--  This site was created in Webflow. https://www.webflow.com  -->
<!--  Last Published: Thu Feb 23 2023 17:19:44 GMT+0000 (Coordinated Universal Time)  -->
<html data-wf-page="63e1d890dd964aaf47ae7881" data-wf-site="63e1d88fdd964a64f8ae787b">
<head>
  <meta charset="utf-8">
  <title>Rubika</title>
  <meta content="Somos RUBIKA, una empresa que fusiona los más de 30 años de GCZ en el mercado con una nueva visión de negocio. La experiencia y conocimientos acumulados durante este tiempo nos permiten ofrecer soluciones personalizadas de acuerdo a las necesidades específicas de cada proyecto, donde la confianza, seguridad y transparencia, sumado a la modernidad, tecnología y nuevo enfoque estratégico nos permite ampliar nuestro portafolio a diversos proyectos en los sectores de educación, salud, transporte, comunicaciones, etc." name="description">
  <meta content="Rubika" property="og:title">
  <meta content="Somos RUBIKA, una empresa que fusiona los más de 30 años de GCZ en el mercado con una nueva visión de negocio. La experiencia y conocimientos acumulados durante este tiempo nos permiten ofrecer soluciones personalizadas de acuerdo a las necesidades específicas de cada proyecto, donde la confianza, seguridad y transparencia, sumado a la modernidad, tecnología y nuevo enfoque estratégico nos permite ampliar nuestro portafolio a diversos proyectos en los sectores de educación, salud, transporte, comunicaciones, etc." property="og:description">
  <meta content="Rubika" property="twitter:title">
  <meta content="Somos RUBIKA, una empresa que fusiona los más de 30 años de GCZ en el mercado con una nueva visión de negocio. La experiencia y conocimientos acumulados durante este tiempo nos permiten ofrecer soluciones personalizadas de acuerdo a las necesidades específicas de cada proyecto, donde la confianza, seguridad y transparencia, sumado a la modernidad, tecnología y nuevo enfoque estratégico nos permite ampliar nuestro portafolio a diversos proyectos en los sectores de educación, salud, transporte, comunicaciones, etc." property="twitter:description">
  <meta property="og:type" content="website">
  <meta content="summary_large_image" name="twitter:card">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="<?php bloginfo('template_directory'); ?>/css/normalize.css" rel="stylesheet" type="text/css">
  <link href="<?php bloginfo('template_directory'); ?>/css/webflow.css" rel="stylesheet" type="text/css">
  <link href="<?php bloginfo('template_directory'); ?>/css/rubika-00645a.webflow.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com" rel="preconnect">
  <link href="https://fonts.gstatic.com" rel="preconnect" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Oswald:200,300,400,500,600,700","Exo:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Playfair Display:regular,500,600,700,800,900,italic,500italic,600italic,700italic,800italic,900italic"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php bloginfo('template_directory'); ?>/images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="<?php bloginfo('template_directory'); ?>/images/webclip.png" rel="apple-touch-icon">
</head>
<body>
  <div id="Top" class="body-content">
    <div class="gradient-section">
      <div class="hero hero2 wf-section">
        <div class="navigation-wrapper">
          <div class="navigation">
            <div class="logo-flex _2">
              <a href="/" aria-current="page" class="logo-wrapper w-inline-block w--current"><img src="<?php bloginfo('template_directory'); ?>/images/FRIDAY-PARTY.png" loading="lazy" width="80" alt="" class="constructo-logo"></a>
            </div>
            <div class="second-part">
              <div data-w-id="1b1828e5-ee8b-a7b3-1723-6ec8ef90000b" class="hamburger">
                <div class="hamburger-menu _2"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625965d7f934b827de7fd133_icon-menu.svg" loading="lazy" alt="" class="dots"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625965e24b264aeb52fab9eb_remove.png" loading="lazy" alt="" class="remove"></div>
                <div class="menu-text-wrapper">
                  <div class="menu-text">MenÚ</div>
                  <div class="menu-text">Cerrar</div>
                </div>
              </div>
            </div>
          </div>
          <div class="triangle-navigation"></div>
          <div class="triange-left left"></div>
          <?php get_template_part('template-parts/menu'); ?>
        </div>
        <div data-delay="4000" data-animation="slide" class="hero hero2 w-slider" data-autoplay="false" data-easing="ease" data-hide-arrows="false" data-disable-swipe="false" data-autoplay-limit="0" data-nav-spacing="3" data-duration="500" data-infinite="true">
          <div class="w-slider-mask">
            <div class="slide-2 w-slide">
              <div class="herocontainer container w-container">
                <div data-w-id="9fea8ae0-28c2-0fb9-4a39-ca32e4bbb09d" style="opacity:0" class="title-1 herosect">MÁS <span class="with-scribble">INFRAESTRUCTURA</span> PARA MÁS PERUANOS</div>
                <a href="#abajo" class="button bother w-inline-block">
                  <div>VER MÁS</div>
                </a>
                <div class="lottie-animation" data-w-id="0b2002b0-49f1-40f4-8d01-5775ba19a6cc" data-animation-type="lottie" data-src="https://uploads-ssl.webflow.com/624c4b185ef5f6159887042a/624d9b4cfb0938ebedc5ecf5_lf30_editor_6govlks1.json" data-loop="1" data-direction="1" data-autoplay="1" data-is-ix2-target="0" data-renderer="svg" data-default-duration="3" data-duration="0"></div>
              </div>
            </div>
            <div class="slide3 w-slide">
              <div class="herocontainer container w-container">
                <div class="title-1 herosect slide2-2">CONECTAMOS A LAS <span class="with-scribble">personas</span> IMPULSANDO SU CRECIMIENTO</div>
                <a href="#abajo" class="button w-inline-block">
                  <div>VER MÁS</div>
                </a>
              </div>
            </div>
            <div class="slide4 w-slide">
              <div class="herocontainer container w-container">
                <div class="title-1 herosect">CONSTRUIMOS <span class="with-scribble">ESPERANZA,</span> DANDO MAYOR ACCESIBILIDAD Y OPORTUNIDADES</div>
                <a href="#abajo" class="button w-inline-block">
                  <div>VER MÁS</div>
                </a>
              </div>
            </div>
          </div>
          <div class="loader">
            <div class="loading-flex">
              <div class="div-block"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/62596961b8f7d1770b41bdae_tail-spin.svg" loading="lazy" width="22" alt="" class="rotator">
                <div class="loadin-flex"><img src="<?php bloginfo('template_directory'); ?>/images/FRIDAY-PARTY.png" loading="lazy" width="200" alt=""></div>
              </div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="icon-3 w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="icon-2 w-icon-slider-right"></div>
          </div>
          <div class="slider3 w-slider-nav w-round"></div>
        </div>
      </div>
      <div id="abajo" class="section wf-section">
        <div class="container">
          <div class="grid-2-columns">
            <div id="w-node-dfe140b9-87fc-626f-7335-7a0275298f02-47ae7881">
              <h1 data-w-id="9e0edf97-2084-82ae-3826-c7134cd7887b" style="opacity:0" class="title-2"><span class="with-scribble">NOSOTROS</span></h1>
              <p class="nosotroshome justify">Somos una empresa especializada en la gestión de diversos proyectos públicos y privados, enfocados en mejorar la calidad de vida de las personas.</p>
              <p class="nosotroshome pequehome justify">Rubika fusiona los más de 30 años de GCZ en el mercado con una nueva visión de negocio. La experiencia y conocimientos acumulados durante este tiempo nos permiten ofrecer soluciones personalizadas de acuerdo a las necesidades específicas de cada reto, donde la confianza, seguridad y transparencia, sumado a la modernidad, tecnología y nuevo enfoque estratégico nos permite ampliar nuestro portafolio a diversos proyectos en los sectores de educación, salud, transporte, comunicaciones, etc.</p>
            </div>
            <div id="w-node-_6023633a-dff0-49e9-7b56-350aa9487ab8-47ae7881">
              <div class="text-wrapper">
                <div class="photo-interaction"><img src="<?php bloginfo('template_directory'); ?>/images/engineer3.jpg" loading="eager" width="666.5" sizes="(max-width: 479px) 91vw, (max-width: 991px) 92vw, (max-width: 1439px) 40vw, 540px" srcset="<?php bloginfo('template_directory'); ?>/images/engineer3-p-500.jpg 500w, <?php bloginfo('template_directory'); ?>/images/engineer3.jpg 738w" alt="" class="photo">
                  <div class="absolute-background"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="margin-150px">
            <div class="max-w-width _2">
              <h1 data-w-id="66a974dc-ffaa-4f61-525e-ad524ff6a84d" style="opacity:0" class="title-2">Nuestras áreas de <span class="with-scribble">ESPECIALIZACIÓN</span></h1>
            </div>
            <div class="grid-3-columns _2">
              <div id="w-node-_65f643f0-a10b-a19f-b521-2d72bc953ba6-47ae7881" data-w-id="65f643f0-a10b-a19f-b521-2d72bc953ba6" style="opacity:0" class="services-block">
                <div class="expertise-circle"><img src="<?php bloginfo('template_directory'); ?>/images/rubikaicon1.png" loading="lazy" width="45" alt=""></div>
                <div class="margin-30px">
                  <h3 class="project-title">Comunicaciones</h3>
                </div>
                <div class="margin-25px">
                  <p class="claimrubro">Soluciones únicas para proyectos de comunicaciones gracias a su enfoque estratégico innovador y tecnológico.</p>
                </div>
              </div>
              <div id="w-node-_0e589080-5c43-3fb8-a708-045461adb5df-47ae7881" data-w-id="0e589080-5c43-3fb8-a708-045461adb5df" style="opacity:0" class="services-block">
                <div class="expertise-circle"><img src="<?php bloginfo('template_directory'); ?>/images/rubikaicon2.png" loading="lazy" width="45" alt=""></div>
                <div class="margin-30px">
                  <h3 class="project-title">salud</h3>
                </div>
                <div class="margin-25px">
                  <p class="claimrubro">Proyectos de infraestructura de salud con un enfoque en confianza, seguridad y transparencia.</p>
                </div>
              </div>
              <div id="w-node-b4bdd155-1519-be3c-9bff-1206af0b0767-47ae7881" data-w-id="b4bdd155-1519-be3c-9bff-1206af0b0767" style="opacity:0" class="services-block">
                <div class="expertise-circle"><img src="<?php bloginfo('template_directory'); ?>/images/rubikaicon3.png" loading="lazy" width="45" alt=""></div>
                <div class="margin-30px">
                  <h3 class="project-title">educación</h3>
                </div>
                <div class="margin-25px">
                  <p class="claimrubro">Realizamos proyectos educativos con décadas de experiencia y conocimiento en el mercado.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="section numeros123 bottomspace2 fondotenue numback wf-section">
        <div class="fs_numbercount-1_component">
          <div class="fs_numbercount-1_embed w-embed w-script">
            <!--  [Finsweet Attributes] Number Count  -->
            <script>(()=>{var t="https://cdn.jsdelivr.net/npm/@finsweet/attributes-numbercount@1/numbercount.js",e=document.querySelector(`script[src="${t}"]`);e||(e=document.createElement("script"),e.async=!0,e.src=t,document.head.append(e));})();</script>
          </div>
          <div class="fs_numbercount-1_wrapper">
            <div class="fs_numbercount-1_card">
              <div id="w-node-ebc7a5d6-1d12-79ec-272c-6f65e5019b3d-91628ccd" class="text-block-9">+</div>
              <div fs-numbercount-element="number" class="fs_numbercount-1_number">30</div>
              <div id="w-node-cf855ef2-6255-2df6-e966-6d1891628cd4-91628ccd" class="fs_numbercount-1_text">Años ayudando a construir un mejor país</div>
            </div>
            <div class="fs_numbercount-1_card">
              <div fs-numbercount-element="number" class="fs_numbercount-1_number">32</div>
              <div id="w-node-cf855ef2-6255-2df6-e966-6d1891628cd9-91628ccd" class="fs_numbercount-1_text">Proyectos desarrollados<br>con calidad y experiencia</div>
              <div id="w-node-_3de0bb02-356f-b0c0-0225-c9226122022b-91628ccd" class="text-block-9">+</div>
            </div>
            <div class="fs_numbercount-1_card">
              <div fs-numbercount-element="number" class="fs_numbercount-1_number">10</div>
              <div id="w-node-cf855ef2-6255-2df6-e966-6d1891628ce0-91628ccd" class="fs_numbercount-1_text">Regiones atendidas en todo el Perú</div>
              <div id="w-node-_65c0cf7e-bdbe-09fc-599b-2748803b2594-91628ccd" class="text-block-9">+</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section hide">
      <div class="max-w-width _3">
        <h1 data-w-id="ed764ccf-d44f-446e-8fac-079d3ceceee0" style="opacity:0" class="title-2 serv34">NUESTRAS <span class="with-scribble">COMPETENCIAS</span></h1>
      </div>
      <div class="grid-3-columns bottomspace">
        <div id="w-node-_2d6059ee-6d8f-f4ad-a383-8a6a3341ab12-47ae7881" class="services-block">
          <div class="expertise-circle"><img src="<?php bloginfo('template_directory'); ?>/images/icons8-construction-256.png" loading="lazy" alt="" class="iconoserv345"></div>
          <h1 class="project-title iconoszona">Planificación</h1>
        </div>
        <div id="w-node-fa111cf3-869a-75e6-11c2-691cd3c50864-47ae7881" class="services-block">
          <div class="expertise-circle">
            <div class="expertise-circle"><img src="<?php bloginfo('template_directory'); ?>/images/icons8-website-builder-256.png" loading="lazy" alt="" class="iconoserv345"></div>
          </div>
          <h1 class="project-title iconoszona">Desarrollo</h1>
        </div>
        <div id="w-node-_0cfc1109-40ba-d0e0-b9cd-4906093356c2-47ae7881" class="services-block">
          <div class="expertise-circle"><img src="<?php bloginfo('template_directory'); ?>/images/icons8-skyscrapers-256.png" loading="lazy" alt="" class="iconoserv345"></div>
          <h1 class="project-title iconoszona">administración</h1>
        </div>
      </div>
    </div>
    <div data-w-id="0499183d-8dcd-0491-96e6-37b4578b8c82" class="color-change-section hide">
      <div class="section lines wf-section">
        <div>
          <div class="max-w-width _3">
            <h1 data-w-id="13c974da-fec1-9ac3-6d27-988da7996442" style="opacity:0" class="title-2">PROYECTOS <span class="with-scribble">EJECUTADOS</span></h1>
          </div>
        </div>
      </div>
      <div class="proyectoshome wf-section">
        <div class="w-dyn-list">
          <div role="list" class="project-list w-dyn-items">
            <div role="listitem" class="w-dyn-item">
              <a href="#" class="work-item-wrapper w-inline-block">
                <div class="overflow-none full"><img src="" loading="eager" data-w-id="7cc52db4-11f9-dfce-09c5-e13822e5a161" alt="" class="work-item-image">
                  <div class="image-cover"></div>
                </div>
                <div class="team-flex">
                  <h4 class="work-item-title"></h4>
                  <p class="work-text"></p>
                </div>
              </a>
            </div>
          </div>
          <div class="w-dyn-empty">
            <div>No items found.</div>
          </div>
        </div>
      </div>
    </div>
    <div class="section hide wf-section">
      <div class="container">
        <div>
          <div class="max-w-width _4">
            <h1 data-w-id="166c8842-1a28-80d4-c03d-ea51ed23c91e" style="opacity:0" class="title-3">As Featured In</h1>
          </div>
          <div class="logo-grid"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625ffc07360c17287180a389_logo-1.png" loading="lazy" width="180" style="opacity:0" id="w-node-_1161822e-4543-0790-7291-612bfc9a97ac-47ae7881" data-w-id="1161822e-4543-0790-7291-612bfc9a97ac" alt=""><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625ffc07360c1727f780a38d_logo-2.png" loading="lazy" width="110" style="opacity:0" id="w-node-_1161822e-4543-0790-7291-612bfc9a97ad-47ae7881" data-w-id="1161822e-4543-0790-7291-612bfc9a97ad" alt="" class="living-logo"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625ffc07360c176bc780a38f_logo-3.png" loading="lazy" width="180" style="opacity:0" id="w-node-_1161822e-4543-0790-7291-612bfc9a97ae-47ae7881" data-w-id="1161822e-4543-0790-7291-612bfc9a97ae" alt=""></div>
          <div class="margin-150px">
            <div class="grid-2-columns reverse">
              <div class="text-wrapper _2">
                <div class="photo-interaction"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625ffe2fdfb1aff24dbed9d9_photo-2.jpg" loading="eager" width="666.5" alt="" class="photo">
                  <div class="absolute-background"></div>
                </div>
              </div>
              <div id="w-node-e9bf0bb1-ac8b-ad33-ace5-d030d1f0efd7-47ae7881">
                <h1 data-w-id="e9bf0bb1-ac8b-ad33-ace5-d030d1f0efd8" style="opacity:0" class="title-2"><span class="with-scribble">Contáctanos</span></h1>
                <div class="margin-60px">
                  <div class="form-block w-form">
                    <form id="wf-form-Call-Me-Back" name="wf-form-Call-Me-Back" data-name="Call Me Back" method="get">
                      <div data-w-id="deb5c7e3-b975-a20d-9fb3-cc4775df1a9a" style="opacity:0" class="contact-flex-page"><label for="name" class="field-label">Tu nombre*</label><input type="text" class="text-field-form w-input" maxlength="256" name="name" data-name="Name" placeholder="" id="name" required=""></div>
                      <div data-w-id="deb5c7e3-b975-a20d-9fb3-cc4775df1a9e" style="opacity:0" class="contact-flex-page"><label for="Email" class="field-label">Tu email*</label><input type="email" class="text-field-form w-input" maxlength="256" name="Email" data-name="Email" placeholder="" id="Email" required=""></div>
                      <div data-w-id="deb5c7e3-b975-a20d-9fb3-cc4775df1aa2" style="opacity:0" class="contact-flex-page"><label for="Phone" class="field-label">Número telefónico*</label><input type="tel" class="text-field-form w-input" maxlength="256" name="Phone" data-name="Phone" placeholder="" id="Phone" required=""></div>
                      <div data-w-id="deb5c7e3-b975-a20d-9fb3-cc4775df1aab" style="opacity:0"><input type="submit" value="ENVIAR" data-wait="Please wait..." class="button full w-button"></div>
                    </form>
                    <div class="success-message w-form-done">
                      <div>Thank you! Your submission has been received!</div>
                    </div>
                    <div class="error-message w-form-fail">
                      <div>Oops! Something went wrong while submitting the form.</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="margin-100px">
              <div class="testimonials-center">
                <div data-w-id="aaa53b1f-e384-23bd-3a6c-aa94099d46c1" style="opacity:0" class="testimonials-content">
                  <div class="testiomonials-photo"></div>
                  <div>
                    <blockquote><strong>“</strong>Creating a landing page with clear and targeted messaging was a crucial step in increasing conversions. Together with the Constructo team, we have compiled a new product page.<strong>”</strong></blockquote>
                    <div class="margin-30px">
                      <div class="role-name">John Doe, <span class="normal">CEO at Webflow</span></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section for-blog hide wf-section">
      <div class="container">
        <div class="max-w-width-flex">
          <h1 data-w-id="ecb2694e-482d-a0af-5f31-ad9ab5f5ebcf" style="opacity:0" class="title-2">Latest <span class="with-scribble">News</span></h1>
          <div data-w-id="5c28629a-acb2-78b8-3643-3838712e93f8" style="opacity:0">
            <a href="pages/news-1.html" class="button w-button">View All</a>
          </div>
        </div>
        <div>
          <div class="w-dyn-list">
            <div role="list" class="blog-post-grid _2 w-dyn-items">
              <div role="listitem" class="w-dyn-item">
                <div data-w-id="54cc35af-6bd5-4710-924c-8ba911af28c1" style="opacity:0" class="blog-wrapper w-clearfix">
                  <a href="#" class="blog-photo _3 w-inline-block"></a>
                  <div class="bottom-part _2">
                    <div class="button-flex-category">
                      <div class="category-text"></div>
                      <div class="button-divider-blog circle"></div>
                      <a href="#" class="category-text category-link"></a>
                    </div>
                    <h3 class="blog-title"></h3>
                  </div>
                </div>
              </div>
            </div>
            <div class="w-dyn-empty">
              <div>No items found.</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <section class="team-slider wf-section">
      <h1 data-w-id="cd97516d-c3de-adf9-779a-00720d9f6803" style="opacity:0" class="title-2 serv34">PROYECTOS <span class="with-scribble">EJECUTADOS</span></h1>
      <div class="columns-3 w-row">
        <div class="links-proyectos w-col w-col-4">
          <a href="/pages/project#salud" class="div-block-5 w-inline-block">
            <h1 class="titulo-pro">Salud</h1>
            <h1 class="subtitulo">Ver proyectos →</h1>
          </a>
        </div>
        <div class="links-proyectos proyecto2 w-col w-col-4">
          <a href="/pages/project#educacion" class="div-block-5 w-inline-block">
            <h1 class="titulo-pro">EDUCACIÓN</h1>
            <h1 class="subtitulo">Ver proyectos →</h1>
          </a>
        </div>
        <div class="links-proyectos proyectos3 w-col w-col-4">
          <a href="/pages/project#sat" class="div-block-5 w-inline-block">
            <h1 class="titulo-pro">SAT</h1>
            <h1 class="subtitulo">Ver proyectos →</h1>
          </a>
        </div>
      </div>
      <div class="container-2"></div>
    </section>


    <!-- <div class="section for-blog wf-section">
      <div class="max-w-width-flex">
        <h1 data-w-id="34d67ffc-21ee-a1f1-c343-f994ebce80f3" style="opacity:0" class="title-2 noticiaslastti">ÚLTIMaS <span class="with-scribble">NOTICIAS</span></h1>
      </div>
      <div>
        <div class="w-dyn-list">
          <div role="list" class="w-dyn-items w-row">
            <div role="listitem" class="w-dyn-item w-col w-col-4">
              <a href="#" class="blog-wrapper w-inline-block"><img height="" loading="lazy" src="" alt="" class="blog-photo">
                <div class="bottom-part _2">
                  <h1 class="blog-title"></h1>
                </div>
              </a>
            </div>
          </div>
          
        </div>
      </div>
    </div> -->
    
    
    
    <div class="section wf-section">
      <div class="container">
        <div>
          <div>
            <div class="grid-2-columns reverse">
              <div class="text-wrapper _2">
                <div class="photo-interaction"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625ffe2fdfb1aff24dbed9d9_photo-2.jpg" loading="eager" width="666.5" alt="" class="photo">
                  <div class="absolute-background"></div>
                </div>
              </div>
              <div id="w-node-a1c5c468-d7ea-dbd6-4a7e-c46fe787b1be-47ae7881">
                <h1 data-w-id="a1c5c468-d7ea-dbd6-4a7e-c46fe787b1bf" style="opacity:0" class="title-2 contactotit"><span class="with-scribble">ESCRÍBENOS</span></h1>
                <div class="margin-60px">
                  <div class="form-block w-form">
                    <form id="wf-form-Call-Me-Back" name="wf-form-Call-Me-Back" data-name="Call Me Back" method="get">
                      <div data-w-id="a1c5c468-d7ea-dbd6-4a7e-c46fe787b1c5" style="opacity:0" class="contact-flex-page"><label for="name-2" class="field-label">TU NOMBRE*</label><input type="text" class="campodetexto w-input" maxlength="256" name="name-2" data-name="Name 2" placeholder="" id="name-2" required=""></div>
                      <div data-w-id="a1c5c468-d7ea-dbd6-4a7e-c46fe787b1c9" style="opacity:0" class="contact-flex-page"><label for="Email-2" class="field-label">TU EMAIL*</label><input type="email" class="campodetexto w-input" maxlength="256" name="Email-2" data-name="Email 2" placeholder="" id="Email-2" required=""></div>
                      <div data-w-id="a1c5c468-d7ea-dbd6-4a7e-c46fe787b1cd" style="opacity:0" class="contact-flex-page"><label for="Phone-2" class="field-label">NÚMERO DE TELÉFONO*</label><input type="tel" class="campodetexto w-input" maxlength="256" name="Phone-2" data-name="Phone 2" placeholder="" id="Phone-2" required=""></div>
                      <div data-w-id="a1c5c468-d7ea-dbd6-4a7e-c46fe787b1d1" style="opacity:0"><input type="submit" value="ENVIAR" data-wait="Please wait..." class="button full formulario w-button"></div>
                    </form>
                    <div class="success-message w-form-done">
                      <div>Thank you! Your submission has been received!</div>
                    </div>
                    <div class="error-message w-form-fail">
                      <div>Oops! Something went wrong while submitting the form.</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="margin-100px">
              <div class="testimonials-center">
                <div data-w-id="a1c5c468-d7ea-dbd6-4a7e-c46fe787b1db" style="opacity:0" class="testimonials-content">
                  <div class="testiomonials-photo"></div>
                  <div>
                    <blockquote><strong>“</strong>Creating a landing page with clear and targeted messaging was a crucial step in increasing conversions. Together with the Constructo team, we have compiled a new product page.<strong>”</strong></blockquote>
                    <div class="margin-30px">
                      <div class="role-name">John Doe, <span class="normal">CEO at Webflow</span></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div data-w-id="cad5cb4a-77f4-ac20-92e5-f6ed5d42479e" class="section-cta image hero wf-section"><img src="<?php bloginfo('template_directory'); ?>/images/SFONDO.png" loading="lazy" alt="" class="photo-section _3">
      <div class="container">
        <h1 data-w-id="cad5cb4a-77f4-ac20-92e5-f6ed5d4247a1" class="title-1 cta">SEGUIMOS CONTRIBUYENDO CON EL DESARROLLO DEL PERÚ</h1>
      </div>
    </div>
    <?php get_template_part('template-parts/footer'); ?>