<?php
/*
Template Name: Proyectos
*/
?>

<!DOCTYPE html><!--  This site was created in Webflow. https://www.webflow.com  -->
<!--  Last Published: Thu Feb 23 2023 17:19:44 GMT+0000 (Coordinated Universal Time)  -->
<html data-wf-page="63e1d890dd964afb2fae7906" data-wf-site="63e1d88fdd964a64f8ae787b">
<head>
  <meta charset="utf-8">
  <title>Constructo - Webflow Ecommerce website template</title>
  <meta content="Constructo - Webflow Ecommerce website template" property="og:title">
  <meta content="Constructo - Webflow Ecommerce website template" property="twitter:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="<?php bloginfo(
      'template_directory'
  ); ?>/css/normalize.css" rel="stylesheet" type="text/css">
  <link href="<?php bloginfo(
      'template_directory'
  ); ?>/css/webflow.css" rel="stylesheet" type="text/css">
  <link href="<?php bloginfo(
      'template_directory'
  ); ?>/css/rubika-00645a.webflow.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com" rel="preconnect">
  <link href="https://fonts.gstatic.com" rel="preconnect" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Oswald:200,300,400,500,600,700","Exo:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Playfair Display:regular,500,600,700,800,900,italic,500italic,600italic,700italic,800italic,900italic"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php bloginfo(
      'template_directory'
  ); ?>/images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="<?php bloginfo('template_directory'); ?>/images/webclip.png" rel="apple-touch-icon">
</head>
<body class="body">
  <div id="Top" class="body-content">
    <div class="gradient-section">
      <div data-w-id="ff13862b-dd00-5733-b584-a24cf35e97ef" class="hero inner">
        <div class="navigation-wrapper">
          <div class="navigation">
            <div class="logo-flex _2">
              <a href="<?php bloginfo(
                  'template_directory'
              ); ?>//" class="logo-wrapper w-inline-block"><img src="<?php bloginfo(
    'template_directory'
); ?>/images/FRIDAY-PARTY.png" loading="lazy" width="80" alt="" class="constructo-logo"></a>
            </div>
            <div class="second-part">
              <div data-w-id="1b1828e5-ee8b-a7b3-1723-6ec8ef90000b" class="hamburger">
                <div class="hamburger-menu _2"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625965d7f934b827de7fd133_icon-menu.svg" loading="lazy" alt="" class="dots"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625965e24b264aeb52fab9eb_remove.png" loading="lazy" alt="" class="remove"></div>
                <div class="menu-text-wrapper">
                  <div class="menu-text">Menu</div>
                  <div class="menu-text">Cerrar</div>
                </div>
              </div>
            </div>
          </div>
          <div class="triangle-navigation"></div>
          <div class="triange-left left"></div>




		  <?php get_template_part('template-parts/menu'); ?>


        </div>
        <div class="container">
          <div class="max-w-hero _2">
            <div data-w-id="ff13862b-dd00-5733-b584-a24cf35e97f3" style="opacity:0" class="title-1 proy"><span class="with-scribble">PROYECTOS</span></div>
          </div>
        </div>
        <div data-w-id="ff13862b-dd00-5733-b584-a24cf35e97fd" style="opacity:0" class="scroll-down-wrapper _1">
          <a href="#" class="scroll-flex w-inline-block">
            <div class="lottie-animation" data-w-id="ff13862b-dd00-5733-b584-a24cf35e97ff" data-animation-type="lottie" data-src="https://uploads-ssl.webflow.com/624c4b185ef5f6159887042a/624d9b4cfb0938ebedc5ecf5_lf30_editor_6govlks1.json" data-loop="1" data-direction="1" data-autoplay="1" data-is-ix2-target="0" data-renderer="svg" data-default-duration="3" data-duration="0"></div>
          </a>
        </div><img src="<?php bloginfo(
            'template_directory'
        ); ?>/images/path-2.png" loading="lazy" sizes="100vw" width="500" srcset="<?php bloginfo(
    'template_directory'
); ?>/images/path-2-p-500.png 500w, <?php bloginfo(
    'template_directory'
); ?>/images/path-2-p-800.png 800w, <?php bloginfo(
    'template_directory'
); ?>/images/path-2-p-1080.png 1080w, <?php bloginfo(
    'template_directory'
); ?>/images/path-2.png 1500w" alt="" class="path _2">
      </div>
    </div>
    <div class="color-change-section">
	<?php
 $categories = get_categories();
 foreach ($categories as $categoria) { ?>
	<div class="wf-section">
        <div>
          <h1 id="<?= $categoria->slug ?>" data-w-id="8bb508a7-43b3-93de-7c56-19c21b210869" style="opacity:0" class="title-2 serv34 proyectospage"><?= $categoria->name ?><span class="with-scribble"></span></h1>
        </div>
      </div>
	  <div class="wf-section">
        <div class="w-dyn-list">
          	<div role="list" class="project-list w-dyn-items">
			<?php
				$args = [
					'post_type' => 'proyectos',
					'cat' => $categoria->term_id,
					'post_status' => 'publish',
				];

				$loop = new WP_Query($args);
				while ($loop->have_posts()):
					$loop->the_post(); ?>
						<!-- Post -->
						<div role="listitem" class="w-dyn-item">
							<a href="<?php the_permalink(); ?>" class="work-item-wrapper w-inline-block">
								<div class="overflow-none full">
                <?php print_r(the_post_thumbnail('medium', array('class' => 'work-item-image', 'loading' => 'lazy'))); ?> 
								<div class="image-cover"></div>
								</div>
								<div class="team-flex">
								<h4 class="work-item-title"><?php the_title();?></h4>
								<p class="work-text"> <?= get_field('ubicacion'); ?> </p>
								</div>
							</a>
						</div>
						<!-- FinPOst -->
				
					<?php
				endwhile;
				?>
				</div>
			</div>
		</div>
		<?php
				}
			?>

     
	</div>
    
  </div>
	<div data-w-id="cad5cb4a-77f4-ac20-92e5-f6ed5d42479e" class="section-cta image hero wf-section"><img src="<?php bloginfo(
     'template_directory'
 ); ?>/images/SFONDO.png" loading="lazy" alt="" class="photo-section _3">
      <div class="container">
        <h1 data-w-id="cad5cb4a-77f4-ac20-92e5-f6ed5d4247a1" class="title-1 cta">SEGUIMOS CONTRIBUYENDO CON EL DESARROLLO DEL PERÚ</h1>
      </div>
    </div>
    <div class="loader">
      <div class="loading-flex">
        <div class="div-block"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/62596961b8f7d1770b41bdae_tail-spin.svg" loading="lazy" width="22" alt="" class="rotator">
          <div class="loadin-flex"><img src="<?php bloginfo(
              'template_directory'
          ); ?>/images/FRIDAY-PARTY.png" loading="lazy" width="200" alt=""></div>
        </div>
      </div>
    </div>
	<?php get_template_part('template-parts/footer'); ?>
