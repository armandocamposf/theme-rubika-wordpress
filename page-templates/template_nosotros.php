<?php 
/*
    Template Name: Plantilla Nosotros
*/ 

?>


<!DOCTYPE html><!--  This site was created in Webflow. https://www.webflow.com  -->
<!--  Last Published: Thu Feb 23 2023 17:19:44 GMT+0000 (Coordinated Universal Time)  -->
<html data-wf-page="63e1d890dd964afb11ae7884" data-wf-site="63e1d88fdd964a64f8ae787b">
<head>
  <meta charset="utf-8">
  <title>Nosotros - Rubika</title>
  <meta content="Nosotros - Rubika" property="og:title">
  <meta content="Nosotros - Rubika" property="twitter:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="<?php bloginfo('template_directory'); ?>/css/normalize.css" rel="stylesheet" type="text/css">
  <link href="<?php bloginfo('template_directory'); ?>/css/webflow.css" rel="stylesheet" type="text/css">
  <link href="<?php bloginfo('template_directory'); ?>/css/rubika-00645a.webflow.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com" rel="preconnect">
  <link href="https://fonts.gstatic.com" rel="preconnect" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Oswald:200,300,400,500,600,700","Exo:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Playfair Display:regular,500,600,700,800,900,italic,500italic,600italic,700italic,800italic,900italic"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php bloginfo('template_directory'); ?>/images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="<?php bloginfo('template_directory'); ?>/images/webclip.png" rel="apple-touch-icon">
</head>
<body class="body">
  <div id="Top" class="body-content">
    <div class="gradient-section">
      <div data-w-id="181102ba-632b-c646-97fb-92c5c265403e" class="section _2">
        <div class="navigation-wrapper">
          <div class="navigation">
            <div class="logo-flex _2">
              <a href="/" class="logo-wrapper w-inline-block"><img src="<?php bloginfo('template_directory'); ?>/images/FRIDAY-PARTY.png" loading="lazy" width="80" alt="" class="constructo-logo"></a>
            </div>
            <div class="second-part">
              <div data-w-id="1b1828e5-ee8b-a7b3-1723-6ec8ef90000b" class="hamburger">
                <div class="hamburger-menu _2"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625965d7f934b827de7fd133_icon-menu.svg" loading="lazy" alt="" class="dots"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625965e24b264aeb52fab9eb_remove.png" loading="lazy" alt="" class="remove"></div>
                <div class="menu-text-wrapper">
                  <div class="menu-text">Menu</div>
                  <div class="menu-text">Cerrar</div>
                </div>
              </div>
            </div>
          </div>
          <div class="triangle-navigation"></div>
          <div class="triange-left left"></div>
          <?php get_template_part('template-parts/menu'); ?>
        </div>
        <div class="container">
          <div class="grid-2-columns reverse">
            <div id="w-node-_8bb1482b-3ac8-18aa-4089-eebe28541247-11ae7884" class="div-block-2">
              <div class="text-wrapper _2">
                <div class="photo-interaction"><img src="<?php bloginfo('template_directory'); ?>/images/SOMOS2-min.png" loading="eager" width="800" sizes="(max-width: 479px) 81vw, (max-width: 991px) 90vw, 32vw" srcset="<?php bloginfo('template_directory'); ?>/images/SOMOS2-min-p-500.png 500w, <?php bloginfo('template_directory'); ?>/images/SOMOS2-min.png 700w" alt="" class="photo foto3 fok">
                  <div class="absolute-background"></div>
                </div>
              </div>
            </div>
            <div id="w-node-_8bb1482b-3ac8-18aa-4089-eebe28541197-11ae7884">
              <h1 data-w-id="8bb1482b-3ac8-18aa-4089-eebe28541198" style="opacity:0" class="title-2">Nuestra <span class="with-scribble">HISTORIA</span></h1>
              <div class="margin-30px">
                <p data-w-id="5fc77fac-ad79-a2b1-1e10-d906a7ba0ce1" style="opacity:0" class="hist3 nosotrostexto historia">En 1991, iniciamos nuestra operación como GCZ Ingenieros S.A.C., especializándonos en la construcción de centrales hidroeléctricas, con el propósito de atender la demanda creciente de infraestructura energética del país, bajo la modalidad EPC y EPCM(Engineering, Procurement, Construction and Management). <br><br>En el 2003, con el objetivo de desarrollar proyectos hidroeléctricos propios expandimos nuestras capacidades de construcción ejecutando obras civiles, subterráneasy proyectos deinterconexión eléctrica. Esto nos llevó, unos años después, a laadquisición de HYRCO S.A.C., empresa especialista en ingeniería,fabricacióny montaje de estructuras metálicas, logrando ampliar nuestra oferta de valor al ofrecer soluciones para diversos proyectos de metalmecánica.<br>‍<br>Buscando nuevos retos, en el 2021, participamosy nos adjudicaron varias licitaciones de proyectos públicos, mediante la modalidad<br>Gobiernoa Gobierno, lo cual nos llevóa formar alianzasestratégicas con diferentes socios para la gestióny desarrollo dediversas obrasy soluciones.<br>‍<br>Hoy, somos RUBIKA, una empresa que ofrece la gestión integral deproyectos públicos, privadosy Obras por Impuestos, respaldadospor más de 30 años de experiencia. Brindamos solucionespersonalizadasa cada requerimiento, con el objetivo de generarmás infraestructuray oportunidades para los peruanos,impulsando su crecimiento, bienestary seguridad.<br></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section vision wf-section">
      <div class="container">
        <div class="grid-2-columns">
          <div id="w-node-d8de821b-960f-90c6-982d-20e4e872fd4b-11ae7884">
            <h1 data-w-id="d8de821b-960f-90c6-982d-20e4e872fd4c" style="opacity:0" class="title-2 mini dark">VISIÓN<span class="with-scribble"></span></h1>
            <p class="paragraph texto345 darkmin">Ser la empresa líder en la región en la gestión de proyectos de infraestructura, generando un impacto positivo en la sociedad.</p>
            <h1 data-w-id="063c9daf-f1a8-153f-f13a-cb8916f3c399" style="opacity:0" class="title-2 mini">MISIÓN<span class="with-scribble"></span></h1>
            <p class="paragraph texto345">Desarrollar y gestionar proyectos que tengan un impacto positivo en la sociedad, mejorando la calidad de vida de las personas.</p>
          </div>
          <div id="w-node-d8de821b-960f-90c6-982d-20e4e872fdfb-11ae7884">
            <div class="text-wrapper">
              <div class="photo-interaction">
                <div class="absolute-background"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="margin-150px">
          <div class="max-w-width _2">
            <h1 data-w-id="d8de821b-960f-90c6-982d-20e4e872fe02" style="opacity:0" class="title-2">Our areas of <span class="with-scribble">expertise</span></h1>
          </div>
          <div class="grid-3-columns _2">
            <div id="w-node-d8de821b-960f-90c6-982d-20e4e872fe07-11ae7884" data-w-id="d8de821b-960f-90c6-982d-20e4e872fe07" style="opacity:0" class="services-block">
              <div class="expertise-circle"><img src="<?php bloginfo('template_directory'); ?>/images/icons8-skyscrapers-256.png" loading="lazy" width="45" alt=""></div>
              <div class="margin-30px">
                <h3 class="project-title">General construction</h3>
              </div>
              <div class="margin-25px">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
              </div>
            </div>
            <div id="w-node-d8de821b-960f-90c6-982d-20e4e872fe10-11ae7884" data-w-id="d8de821b-960f-90c6-982d-20e4e872fe10" style="opacity:0" class="services-block">
              <div class="expertise-circle"><img src="<?php bloginfo('template_directory'); ?>/images/icons8-website-builder-256.png" loading="lazy" width="45" alt=""></div>
              <div class="margin-30px">
                <h3 class="project-title">Design build</h3>
              </div>
              <div class="margin-25px">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
              </div>
            </div>
            <div id="w-node-d8de821b-960f-90c6-982d-20e4e872fe19-11ae7884" data-w-id="d8de821b-960f-90c6-982d-20e4e872fe19" style="opacity:0" class="services-block">
              <div class="expertise-circle"><img src="<?php bloginfo('template_directory'); ?>/images/icons8-construction-256.png" loading="lazy" width="45" alt=""></div>
              <div class="margin-30px">
                <h3 class="project-title">Project management</h3>
              </div>
              <div class="margin-25px">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="margin-150px">
          <div class="max-w-width _4">
            <h1 data-w-id="6cf50ea1-f9b9-a656-f283-f18f40b4a07c" style="opacity:0" class="title-3">As Featured In</h1>
          </div>
          <div class="logo-grid"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625ffc07360c17287180a389_logo-1.png" loading="lazy" width="200" style="opacity:0" id="w-node-_6cf50ea1-f9b9-a656-f283-f18f40b4a07f-11ae7884" data-w-id="6cf50ea1-f9b9-a656-f283-f18f40b4a07f" alt=""><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625ffc07360c1727f780a38d_logo-2.png" loading="lazy" width="125" style="opacity:0" id="w-node-_6cf50ea1-f9b9-a656-f283-f18f40b4a080-11ae7884" data-w-id="6cf50ea1-f9b9-a656-f283-f18f40b4a080" alt="" class="living-logo"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625ffc07360c176bc780a38f_logo-3.png" loading="lazy" width="200" style="opacity:0" id="w-node-_6cf50ea1-f9b9-a656-f283-f18f40b4a081-11ae7884" data-w-id="6cf50ea1-f9b9-a656-f283-f18f40b4a081" alt=""></div>
          <div class="margin-150px">
            <div class="max-w-width _2">
              <h1 data-w-id="45d7aab8-6bf3-1a96-aa5b-4de1298e2e16" style="opacity:0" class="title-2">Meet the <span class="with-scribble">team</span></h1>
            </div>
            <div>
              <div data-w-id="17108d8d-8680-9a29-cd4f-2e86c75c3317" style="opacity:0" class="team-grid">
                <div id="w-node-_17108d8d-8680-9a29-cd4f-2e86c75c3318-11ae7884" data-w-id="17108d8d-8680-9a29-cd4f-2e86c75c3318" style="opacity:0" class="team-content">
                  <div class="author-photo"></div>
                  <div class="margin-30px">
                    <div class="team-name">John Doe, Founder</div>
                  </div>
                  <div class="margin-25px">
                    <div class="social-wrapper-large">
                      <div class="team-social-flex">
                        <a href="#" class="social-link-1 w-inline-block"><img src="https://uploads-ssl.webflow.com/61d7143a2905d273d92bb172/61d8312abe9d8d754aba8c37_instagram.png" loading="lazy" alt=""></a>
                        <a href="#" class="social-link-1 w-inline-block"><img src="https://uploads-ssl.webflow.com/61d7143a2905d273d92bb172/61d83129e08587f8794ad9e5_linkedin.png" loading="lazy" alt=""></a>
                        <a href="#" class="social-link-1 w-inline-block"><img src="https://uploads-ssl.webflow.com/61d7143a2905d273d92bb172/61d831296d1a1a9a233f2f6f_twiiter.png" loading="lazy" alt=""></a>
                        <a href="#" class="social-link-1 w-inline-block"><img src="https://uploads-ssl.webflow.com/61d7143a2905d273d92bb172/61d831296a4c64046f564735_facebook.png" loading="lazy" alt=""></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="w-node-_17108d8d-8680-9a29-cd4f-2e86c75c332a-11ae7884" data-w-id="17108d8d-8680-9a29-cd4f-2e86c75c332a" style="opacity:0" class="team-content">
                  <div class="author-photo _2"></div>
                  <div class="margin-30px">
                    <div class="team-name">Rebeca Hughe, archtiect</div>
                  </div>
                  <div class="margin-25px">
                    <div class="social-wrapper-large">
                      <div class="team-social-flex">
                        <a href="#" class="social-link-1 w-inline-block"><img src="https://uploads-ssl.webflow.com/61d7143a2905d273d92bb172/61d8312abe9d8d754aba8c37_instagram.png" loading="lazy" alt=""></a>
                        <a href="#" class="social-link-1 w-inline-block"><img src="https://uploads-ssl.webflow.com/61d7143a2905d273d92bb172/61d83129e08587f8794ad9e5_linkedin.png" loading="lazy" alt=""></a>
                        <a href="#" class="social-link-1 w-inline-block"><img src="https://uploads-ssl.webflow.com/61d7143a2905d273d92bb172/61d831296d1a1a9a233f2f6f_twiiter.png" loading="lazy" alt=""></a>
                        <a href="#" class="social-link-1 w-inline-block"><img src="https://uploads-ssl.webflow.com/61d7143a2905d273d92bb172/61d831296a4c64046f564735_facebook.png" loading="lazy" alt=""></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="w-node-_17108d8d-8680-9a29-cd4f-2e86c75c333c-11ae7884" data-w-id="17108d8d-8680-9a29-cd4f-2e86c75c333c" style="opacity:0" class="team-content">
                  <div class="author-photo _3"></div>
                  <div class="margin-30px">
                    <div class="team-name">Justin Lewis, interior designer</div>
                  </div>
                  <div class="margin-25px">
                    <div class="social-wrapper-large">
                      <div class="team-social-flex">
                        <a href="#" class="social-link-1 w-inline-block"><img src="https://uploads-ssl.webflow.com/61d7143a2905d273d92bb172/61d8312abe9d8d754aba8c37_instagram.png" loading="lazy" alt=""></a>
                        <a href="#" class="social-link-1 w-inline-block"><img src="https://uploads-ssl.webflow.com/61d7143a2905d273d92bb172/61d83129e08587f8794ad9e5_linkedin.png" loading="lazy" alt=""></a>
                        <a href="#" class="social-link-1 w-inline-block"><img src="https://uploads-ssl.webflow.com/61d7143a2905d273d92bb172/61d831296d1a1a9a233f2f6f_twiiter.png" loading="lazy" alt=""></a>
                        <a href="#" class="social-link-1 w-inline-block"><img src="https://uploads-ssl.webflow.com/61d7143a2905d273d92bb172/61d831296a4c64046f564735_facebook.png" loading="lazy" alt=""></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="margin-150px">
            <div class="grid-2-columns reverse">
              <div class="text-wrapper _2">
                <div class="photo-interaction"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625ffe2fdfb1aff24dbed9d9_photo-2.jpg" loading="eager" width="666.5" alt="" class="photo">
                  <div class="absolute-background"></div>
                </div>
              </div>
              <div id="w-node-_6cf50ea1-f9b9-a656-f283-f18f40b4a088-11ae7884">
                <h1 data-w-id="6cf50ea1-f9b9-a656-f283-f18f40b4a089" style="opacity:0" class="title-2">need a <span class="with-scribble">quote</span></h1>
                <div class="margin-60px">
                  <div class="form-block w-form">
                    <form id="wf-form-Call-Me-Back" name="wf-form-Call-Me-Back" data-name="Call Me Back" method="get">
                      <div data-w-id="6cf50ea1-f9b9-a656-f283-f18f40b4a090" style="opacity:0" class="contact-flex-page"><label for="Name-2" class="field-label">Full Name*</label><input type="text" class="text-field-form w-input" maxlength="256" name="Name" data-name="Name" placeholder="" id="Name-2" required=""></div>
                      <div data-w-id="6cf50ea1-f9b9-a656-f283-f18f40b4a094" style="opacity:0" class="contact-flex-page"><label for="Email-2" class="field-label">Email Address*</label><input type="email" class="text-field-form w-input" maxlength="256" name="Email" data-name="Email" placeholder="" id="Email-2" required=""></div>
                      <div data-w-id="6cf50ea1-f9b9-a656-f283-f18f40b4a098" style="opacity:0" class="contact-flex-page"><label for="Phone-2" class="field-label">phone number*</label><input type="tel" class="text-field-form w-input" maxlength="256" name="Phone" data-name="Phone" placeholder="" id="Phone-2" required=""></div>
                      <div data-w-id="6cf50ea1-f9b9-a656-f283-f18f40b4a09c" style="opacity:0"><input type="submit" value="Call me back" data-wait="Please wait..." class="button full w-button"></div>
                    </form>
                    <div class="success-message w-form-done">
                      <div>Thank you! Your submission has been received!</div>
                    </div>
                    <div class="error-message w-form-fail">
                      <div>Oops! Something went wrong while submitting the form.</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section wf-section">
      <div class="max-w-width nobottom">
        <h1 data-w-id="ab5eb80d-39fe-8d42-56be-e6bb2c38273d" style="opacity:0" class="title-2">NuestrOS <span class="with-scribble">VALORES</span></h1>
      </div>
      <div class="w-row">
        <div class="w-col w-col-4">
          <div class="columnasvalor"><img src="<?php bloginfo('template_directory'); ?>/images/secure-shield.png" loading="lazy" alt="" class="iconovalor">
            <div class="valores">LA SEGURIDAD ES PRIMERO</div><img src="<?php bloginfo('template_directory'); ?>/images/partnership.png" loading="lazy" alt="" class="iconovalor">
            <div class="valores">NUESTRO EQUIPO LO ES TODO</div>
          </div>
        </div>
        <div class="w-col w-col-4">
          <div class="columnasvalor"><img src="<?php bloginfo('template_directory'); ?>/images/relationship.png" loading="lazy" alt="" class="iconovalor">
            <div class="valores">LA INTEGRIDAD ES LA BASE</div><img src="<?php bloginfo('template_directory'); ?>/images/hands.png" loading="lazy" alt="" class="iconovalor">
            <div class="valores">ENTENDEMOS Y RESPETAMOS POR IGUAL</div>
          </div>
        </div>
        <div class="w-col w-col-4">
          <div class="columnasvalor"><img src="<?php bloginfo('template_directory'); ?>/images/excellence.png" loading="lazy" alt="" class="iconovalor">
            <div class="valores">PERSEGUIMOS LA EXCELENCIA</div><img src="<?php bloginfo('template_directory'); ?>/images/goal.png" loading="lazy" alt="" class="iconovalor">
            <div class="valores">NOS ADAPTAMOS A LOS DESAFÍOS</div>
          </div>
        </div>
      </div>
    </div>
    <div class="section numeros123 bottomspace2 fondotenue numback wf-section">
      <div class="fs_numbercount-1_component">
        <div class="fs_numbercount-1_embed w-embed w-script">
          <!--  [Finsweet Attributes] Number Count  -->
          <script>(()=>{var t="https://cdn.jsdelivr.net/npm/@finsweet/attributes-numbercount@1/numbercount.js",e=document.querySelector(`script[src="${t}"]`);e||(e=document.createElement("script"),e.async=!0,e.src=t,document.head.append(e));})();</script>
        </div>
        <div class="fs_numbercount-1_wrapper">
          <div class="fs_numbercount-1_card">
            <div id="w-node-ebc7a5d6-1d12-79ec-272c-6f65e5019b3d-91628ccd" class="text-block-9">+</div>
            <div fs-numbercount-element="number" class="fs_numbercount-1_number">30</div>
            <div id="w-node-cf855ef2-6255-2df6-e966-6d1891628cd4-91628ccd" class="fs_numbercount-1_text">Años ayudando a construir un mejor país</div>
          </div>
          <div class="fs_numbercount-1_card">
            <div fs-numbercount-element="number" class="fs_numbercount-1_number">32</div>
            <div id="w-node-cf855ef2-6255-2df6-e966-6d1891628cd9-91628ccd" class="fs_numbercount-1_text">Proyectos desarrollados<br>con calidad y experiencia</div>
            <div id="w-node-_3de0bb02-356f-b0c0-0225-c9226122022b-91628ccd" class="text-block-9">+</div>
          </div>
          <div class="fs_numbercount-1_card">
            <div fs-numbercount-element="number" class="fs_numbercount-1_number">10</div>
            <div id="w-node-cf855ef2-6255-2df6-e966-6d1891628ce0-91628ccd" class="fs_numbercount-1_text">Regiones atendidas en todo el Perú</div>
            <div id="w-node-_65c0cf7e-bdbe-09fc-599b-2748803b2594-91628ccd" class="text-block-9">+</div>
          </div>
        </div>
      </div>
    </div>
    <div data-w-id="cad5cb4a-77f4-ac20-92e5-f6ed5d42479e" class="section-cta image hero wf-section"><img src="<?php bloginfo('template_directory'); ?>/images/SFONDO.png" loading="lazy" alt="" class="photo-section _3">
      <div class="container">
        <h1 data-w-id="cad5cb4a-77f4-ac20-92e5-f6ed5d4247a1" class="title-1 cta">SEGUIMOS CONTRIBUYENDO CON EL DESARROLLO DEL PERÚ</h1>
      </div>
    </div>
    <div class="loader">
      <div class="loading-flex">
        <div class="div-block"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/62596961b8f7d1770b41bdae_tail-spin.svg" loading="lazy" width="22" alt="" class="rotator">
          <div class="loadin-flex"><img src="<?php bloginfo('template_directory'); ?>/images/FRIDAY-PARTY.png" loading="lazy" width="200" alt=""></div>
        </div>
      </div>
    </div>
    <?php get_template_part('template-parts/footer'); ?>