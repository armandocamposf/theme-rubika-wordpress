<?php 
/*
    Template Name: Plantilla Contacto
*/ 

?>

<!DOCTYPE html>
<!--  This site was created in Webflow. https://www.webflow.com  -->
<!--  Last Published: Thu Feb 23 2023 17:19:44 GMT+0000 (Coordinated Universal Time)  -->
<html data-wf-page="63e1d890dd964a78aeae788f" data-wf-site="63e1d88fdd964a64f8ae787b">
<head>
 <meta charset="utf-8">
 <title>Contacto - Rubika</title>
 <meta content="Contacto - Rubika" property="og:title">
 <meta content="Contacto - Rubika" property="twitter:title">
 <meta content="width=device-width, initial-scale=1" name="viewport">
 <meta content="Webflow" name="generator">
 <link href="<?php bloginfo('template_directory'); ?>/css/normalize.css" rel="stylesheet" type="text/css">
 <link href="<?php bloginfo('template_directory'); ?>/css/webflow.css" rel="stylesheet" type="text/css">
 <link href="<?php bloginfo('template_directory'); ?>/css/rubika-00645a.webflow.css" rel="stylesheet" type="text/css">
 <link href="https://fonts.googleapis.com" rel="preconnect">
 <link href="https://fonts.gstatic.com" rel="preconnect" crossorigin="anonymous">
 <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
 <script type="text/javascript">
  WebFont.load({  google: {    families: ["Oswald:200,300,400,500,600,700","Exo:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Playfair Display:regular,500,600,700,800,900,italic,500italic,600italic,700italic,800italic,900italic"]  }});
 </script>
 <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
 <script type="text/javascript">
  !function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);
 </script>
 <link href="<?php bloginfo('template_directory'); ?>/images/favicon.png" rel="shortcut icon" type="image/x-icon">
 <link href="<?php bloginfo('template_directory'); ?>/images/webclip.png" rel="apple-touch-icon">
</head>
<body class="body">
 <div id="Top" class="body-content">
  <div class="gradient-section">
   <div data-w-id="88db2853-e2bf-810a-502e-e4269fdaa9d2" class="hero inner overflow">
    <div class="navigation-wrapper">
     <div class="navigation">
      <div class="logo-flex _2">
       <a href="/" class="logo-wrapper w-inline-block"><img src="<?php bloginfo('template_directory'); ?>/images/FRIDAY-PARTY.png" loading="lazy" width="80" alt="" class="constructo-logo"></a>
      </div>
      <div class="second-part">
       <div data-w-id="1b1828e5-ee8b-a7b3-1723-6ec8ef90000b" class="hamburger">
        <div class="hamburger-menu _2"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625965d7f934b827de7fd133_icon-menu.svg" loading="lazy" alt="" class="dots"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/625965e24b264aeb52fab9eb_remove.png" loading="lazy" alt="" class="remove"></div>
        <div class="menu-text-wrapper">
         <div class="menu-text">Menu</div>
         <div class="menu-text">Cerrar</div>
        </div>
       </div>
      </div>
     </div>
     <div class="triangle-navigation"></div>
     <div class="triange-left left"></div>
     <?php get_template_part('template-parts/menu'); ?>
    </div>
    <div class="container">
     <div class="max-w-hero _2">
      <div data-w-id="88db2853-e2bf-810a-502e-e4269fdaa9d6" style="opacity:0" class="title-1 contacto">CONTÁCTANOS<span class="with-scribble"></span></div>
     </div>
    </div>
    <div data-w-id="88db2853-e2bf-810a-502e-e4269fdaa9da" style="opacity:0" class="scroll-down-wrapper _1">
     <a href="#" class="scroll-flex w-inline-block">
      <div class="lottie-animation" data-w-id="88db2853-e2bf-810a-502e-e4269fdaa9dc" data-animation-type="lottie" data-src="https://uploads-ssl.webflow.com/624c4b185ef5f6159887042a/624d9b4cfb0938ebedc5ecf5_lf30_editor_6govlks1.json" data-loop="1" data-direction="1" data-autoplay="1" data-is-ix2-target="0" data-renderer="svg" data-default-duration="3" data-duration="0"></div>
     </a>
    </div><img src="<?php bloginfo('template_directory'); ?>/images/path-2.png" loading="lazy" sizes="100vw" width="500" srcset="<?php bloginfo('template_directory'); ?>/images/path-2-p-500.png 500w, <?php bloginfo('template_directory'); ?>/images/path-2-p-800.png 800w, <?php bloginfo('template_directory'); ?>/images/path-2-p-1080.png 1080w, <?php bloginfo('template_directory'); ?>/images/path-2.png 1500w" alt="" class="path _2">
   </div>
  </div>
  <div class="section wf-section">
   <div class="container">
    <div>
     <div>
      <div class="grid-2-columns reverse">
       <div class="text-wrapper _2">
        <div class="photo-interaction"><img src="<?php bloginfo('template_directory'); ?>/images/Contacto-1-min.jpeg" loading="eager" width="666.5" sizes="(max-width: 479px) 81vw, (max-width: 991px) 90vw, 32vw" srcset="<?php bloginfo('template_directory'); ?>/images/Contacto-1-min-p-500.jpeg 500w, <?php bloginfo('template_directory'); ?>/images/Contacto-1-min-p-800.jpeg 800w, <?php bloginfo('template_directory'); ?>/images/Contacto-1-min.jpeg 887w" alt="" class="photo">
         <div class="absolute-background"></div>
        </div>
       </div>
       <div id="w-node-e8f1efa6-6d31-31f9-df29-bde4f39ece26-aeae788f">
        <div class="margin-60px">
         <div class="form-block w-form">
          <form id="wf-form-Mensaje" name="wf-form-Mensaje" data-name="Mensaje" method="get">
           <div data-w-id="e8f1efa6-6d31-31f9-df29-bde4f39ece2e" style="opacity:0" class="contact-flex-page"><label for="name-2" class="field-label">NOMBRE*</label><input type="text" class="text-field-form w-input" maxlength="256" name="name-2" data-name="Name 2" placeholder="" id="name-2" required=""></div>
           <div data-w-id="e8f1efa6-6d31-31f9-df29-bde4f39ece32" style="opacity:0" class="contact-flex-page"><label for="Email-2" class="field-label">EMAIL*</label><input type="email" class="text-field-form w-input" maxlength="256" name="Email-2" data-name="Email 2" placeholder="" id="Email-2" required=""></div>
           <div data-w-id="e8f1efa6-6d31-31f9-df29-bde4f39ece36" style="opacity:0" class="contact-flex-page"><label for="Phone-2" class="field-label">TELÉFONO*</label><input type="tel" class="text-field-form w-input" maxlength="256" name="Phone-2" data-name="Phone 2" placeholder="" id="Phone-2" required=""></div>
           <div data-w-id="012501b1-a5f8-0072-8607-5e0b925cae31" style="opacity:0" class="contact-flex-page"><label for="field" class="field-label">Mensaje</label><textarea placeholder="" maxlength="5000" id="field" name="field" data-name="Field" class="text-field-form w-input"></textarea></div>
           <div data-w-id="e8f1efa6-6d31-31f9-df29-bde4f39ece3a" style="opacity:0"><input type="submit" value="ENVIAR" data-wait="Please wait..." class="button full formulario _234 w-button"></div>
          </form>
          <div class="success-message w-form-done">
           <div class="text-block-8">GRacias por contactarnos, te responderemos lo más pronto posible.</div>
          </div>
          <div class="error-message w-form-fail">
           <div>Oops! Something went wrong while submitting the form.</div>
          </div>
         </div>
        </div>
       </div>
      </div>
      <div class="margin-100px">
       <div class="testimonials-center">
        <div data-w-id="e8f1efa6-6d31-31f9-df29-bde4f39ece44" style="opacity:0" class="testimonials-content">
         <div class="testiomonials-photo"></div>
         <div>
          <blockquote><strong>“</strong>Creating a landing page with clear and targeted messaging was a crucial step in increasing conversions. Together with the Constructo team, we have compiled a new product page.<strong>”</strong></blockquote>
          <div class="margin-30px">
           <div class="role-name">John Doe, <span class="normal">CEO at Webflow</span></div>
          </div>
         </div>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
  <div class="loader">
   <div class="loading-flex">
    <div class="div-block"><img src="https://uploads-ssl.webflow.com/62594e5c6f2deb0d0b80fe9c/62596961b8f7d1770b41bdae_tail-spin.svg" loading="lazy" width="22" alt="" class="rotator">
     <div class="loadin-flex"><img src="<?php bloginfo('template_directory'); ?>/images/FRIDAY-PARTY.png" loading="lazy" width="200" alt=""></div>
    </div>
   </div>
  </div>
  <?php get_template_part('template-parts/footer'); ?>