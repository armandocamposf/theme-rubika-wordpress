<!DOCTYPE html><!--  This site was created in Webflow. https://www.webflow.com  -->
<!--  Last Published: Thu Feb 23 2023 17:19:44 GMT+0000 (Coordinated Universal Time)  -->
<html data-wf-page="63e1d890dd964a78aeae788f" data-wf-site="63e1d88fdd964a64f8ae787b">
<head>
  <meta charset="utf-8">
  <title>Contacto - Rubika</title>
  <meta content="Contacto - Rubika" property="og:title">
  <meta content="Contacto - Rubika" property="twitter:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="<?php bloginfo(
      'template_directory'
  ); ?>/css/normalize.css" rel="stylesheet" type="text/css">
  <link href="<?php bloginfo('template_directory'); ?>/css/webflow.css" rel="stylesheet" type="text/css">
  <link href="<?php bloginfo(
      'template_directory'
  ); ?>/css/rubika-00645a.webflow.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com" rel="preconnect">
  <link href="https://fonts.gstatic.com" rel="preconnect" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Oswald:200,300,400,500,600,700","Exo:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Playfair Display:regular,500,600,700,800,900,italic,500italic,600italic,700italic,800italic,900italic"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php bloginfo(
      'template_directory'
  ); ?>/images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="<?php bloginfo(
      'template_directory'
  ); ?>/images/webclip.png" rel="apple-touch-icon">
</head>
<body class="body">
  <div id="Top" class="body-content">
    <div class="gradient-section">
      <div data-w-id="88db2853-e2bf-810a-502e-e4269fdaa9d2" class="hero inner overflow">
        <?php get_template_part('template-parts/header/menu'); ?>
        <?php get_template_part('template-parts/header/encabezado'); ?>
        </div>
    </div></div>