<?php
//esto sirve para que si o si reconozca la raiz del single-proyectos.php
flush_rewrite_rules();
//verificar que sea Wordpress superior a 5.3
if (version_compare($GLOBALS['wp_version'], '5.3', '<')) {
    require get_template_directory() . '/inc/back-compat.php';
}

add_theme_support( 'post-thumbnails' );
